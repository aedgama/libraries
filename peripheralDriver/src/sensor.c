/*************************************************************************//**
  @file     sensor.c
  @brief    Fuction to use the sensor in the PCB
  @author   Marcos Darino (MD)
 *****************************************************************************/


/** \addtogroup DRIVER_GROUP Peripheral Drivers
 ** @{ */

/** \addtogroup SENSOR_GROUP Sensor
 ** @{ */

/*==================[inclusions]=============================================*/
#include "sensor.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/**
 * @brief Sensor constructor: Clean all the Sensor
 */
void sensorConstructor (sensor_t *sensor)
{
  sensor->type=SEN_NULL;
  sensor->config=SEN_NULL;
  sensor->data=0;
  sensor->alarm=0;
  sensor->counter=0;
}

/**
 * @brief Sensor Init: Set the type and clean the data
 * @param sensor    Struct of the sensor
 * @param type      type of the sensor
 */
void sensorInit (sensor_t *sensor, uint8_t type, uint8_t config)
{
  sensor->type=type;
  sensor->config=config;
  sensor->data=0;
  sensor->alarm=0;
  sensor->counter=0;
}
/**
 * @brief Sensor Get Type: Return the Type
 */
uint8_t sensorGetType (sensor_t *sensor)
{
  return (sensor->type);
}

/**
 * @brief Sensor Set Data: Update the value of the data
 */
void sensorSetData (sensor_t *sensor, uint16_t data)
{
  sensor->data=data;
}
/**
 * @brief Sensor Get Data: Return the value of the data
 */
uint16_t sensorGetData (sensor_t *sensor)
{
  return (sensor->data);
}

/**
 * @brief Sensor Set Alarm: Update the value of the Alarm
 */
void sensorSetAlarm (sensor_t *sensor, uint16_t alarm)
{
  sensor->alarm=alarm;
}
/**
 * @brief Sensor Get Alarm: Return the value of the Alarm
 */
uint16_t sensorGetAlarm (sensor_t *sensor)
{
  return (sensor->alarm);
}

/**
 * @brief Sensor Set Counter: Update the value of the Counter
 */
void sensorSetCounter (sensor_t *sensor, uint16_t counter)
{
  sensor->counter=counter;
}

/**
 * @brief Sensor Get Counter: Return the value of the Counter
 */
uint16_t sensorGetCounter (sensor_t *sensor)
{
  return (sensor->counter);
}

/*==================[internal functions definition]==========================*/
/*==================[external functions definition]==========================*/

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

