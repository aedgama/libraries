/*************************************************************************//**
  @file     led.c
  @brief    led fuctions
  @author   Marcos Darino (MD)
  ******************************************************************************/


/** \addtogroup DRIVER_GROUP Peripheral Drivers
 ** @{ */


/** \addtogroup LED_GROUP led
 ** @{ */



/*==================[inclusions]=============================================*/
#include "leds.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/


/*==================[external functions definition]==========================*/


/**
 * @brief Create the led "object" - Define the GPIO and his bit
 */
void ledConstructor (led_t *led, uint8_t gpio, uint8_t gpio_bit)
{
	led->state=UNDEFINED;
	led->gpio=gpio;
	led->gpio_bit=gpio_bit;
}
/**
 * @brief Init of the led, Configure the Pin function and set it as input
 */
void ledInit (led_t *led, uint8_t  port, uint8_t  port_pin ,uint8_t function)
{
	GPIO_pinConfiguration(port,port_pin,SCU_MODE_INACT ,function);  /* GPIO5[0], LED0R */
	GPIO_setAsOutput(led->gpio,led->gpio_bit);
}

/*Set ON the led*/
void led_SetON (led_t *led)
{
	GPIO_setON(led->gpio,led->gpio_bit);
}
/*Set OFF the led*/
void led_SetOFF (led_t *led)
{
	GPIO_SetOFF(led->gpio,led->gpio_bit);
}

/*Set Toggle the led*/
void led_SetToggle (led_t *led)
{
	GPIO_Toggle(led->gpio,led->gpio_bit);
}


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/



