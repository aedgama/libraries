/*************************************************************************//**
  @file     relay.c
  @brief    Relay fuctions
  @author   Marcos Darino (MD)
  ******************************************************************************/


/** \addtogroup DRIVER_GROUP Peripheral Drivers
 ** @{ */


/** \addtogroup RELAY_GROUP Relay
 ** @{ */



/*==================[inclusions]=============================================*/
#include "relay.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/


/*==================[external functions definition]==========================*/


/**
 * @brief Create the relay "object" - Define the GPIO and his bit
 */
void relayConstructor (relay_t *relay, uint8_t gpio, uint8_t gpio_bit)
{
	relay->state = UNDEFINED;
	relay->gpio = gpio;
	relay->gpio_bit = gpio_bit;
}
/**
 * @brief Init of the relay, Configure the Pin function and set it as input
 */
void relayInit (relay_t *relay, uint8_t  port, uint8_t  port_pin , uint8_t function)
{
	GPIO_pinConfiguration(port, port_pin, SCU_MODE_INACT , function); /* GPIO5[0], LED0R */
	GPIO_setAsOutput(relay->gpio, relay->gpio_bit);
}

/*Set ON the relay*/
void relay_SetON (relay_t *relay)
{
	GPIO_setON(relay->gpio, relay->gpio_bit);
}
/*Set OFF the relay*/
void relay_SetOFF (relay_t *relay)
{
	GPIO_SetOFF(relay->gpio, relay->gpio_bit);
}

/*Set Toggle the relay*/
void relay_SetToggle (relay_t *relay)
{
	GPIO_Toggle(relay->gpio, relay->gpio_bit);
}


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

