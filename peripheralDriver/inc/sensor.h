/************************************************************************//**
  @file     sensor.h
  @brief    Fuction to use the sensor in the PCB
  @author   Marcos Darino (MD)
  @version  20160607 v0.0.1   MD first version

 ******************************************************************************/

#ifndef _SENSOR_H_
#define _SENSOR_H_



/** \addtogroup DRIVER_GROUP Peripheral Drivers
 ** @{ */

/** \addtogroup SENSOR_GROUP Sensor
 ** @{ */


/*==================[inclusions]=============================================*/
#include "chip.h"
/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

//--  0 to 9 -- // reserved characters
#define  SEN_NULL            0
//--  10 -- // Temperature sensors
#define  SEN_TEMPERATURE     10
//--  11 -- // Current sensors
#define  SEN_CURRENT         11 
//--  12 -- // Input sensors
#define  SEN_INPUT           12
//--  13 -- // Red color sensors
#define  SEN_COLOR_RED       13  
//--  14 -- // Blue color sensors
#define  SEN_COLOR_BLUE      14 
//--  15 -- // Green color sensors
#define  SEN_COLOR_GREEN     15  
//--  16 -- // Light sensors
#define  SEN_LIGHT           16 
//--  17 -- // Accelerometer x  sensors
#define  SEN_ACCEL_X         17
//--  18 -- // Accelerometer y  sensors
#define  SEN_ACCEL_Y         18
//--  19 -- // Accelerometer z  sensors
#define  SEN_ACCEL_Z         19   

//-- ALARMS -- //
#define  SEN_ALARM_NULL         0
//-- COUNTER -- //   
#define  SEN_COUNTER_NULL       0


/*==================[typedef]================================================*/

typedef struct STR_Sensor
{
   uint8_t  type;
   uint8_t  config;
   int16_t  data;
   int16_t  alarm;
   int16_t  counter;
}sensor_t;


/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/**
 * @brief Sensor constructor: Clean all the Sensor
 * @param sensor   Struct of the sensor
 */
void sensorConstructor (sensor_t *sensor);

/**
 * @brief Sensor Init: Set the type and clean the data
 * @param sensor   Struct of the sensor
 * @param type     Type of the sensor
 * @param config   Configuration of the sensor
 */
void sensorInit (sensor_t *sensor, uint8_t type, uint8_t config);


/**
 * @brief   Sensor Get Type: Return the Type
 * @return  Type of the sensor
 */
uint8_t sensorGetType (sensor_t *sensor);



/**
 * @brief Sensor Set Data: Update the value of the data
 * @param sensor   Struct of the sensor
 * @param data     data of the sensor
 */
void sensorSetData (sensor_t *sensor, uint16_t data);

/**
 * @brief Sensor Get Data: Return the value of the data
 * @param   sensor   Struct of the sensor
 * @return  data of the sensor
 */
uint16_t sensorGetData (sensor_t *sensor);


/**
 * @brief Sensor Set Alarm: Update the value of the Alarm
 * @param   sensor   Struct of the sensor
 * @param   alarm    alarm of the sensor
 */
 
void sensorSetAlarm (sensor_t *sensor, uint16_t alarm);

/**
 * @brief Sensor Get Alarm: Return the value of the Alarm
 * @param   sensor   Struct of the sensor
 * @return  alarm of the sensor
 */
uint16_t sensorGetAlarm (sensor_t *sensor);


/**
 * @brief Sensor Set Counter: Update the value of the Counter
 * @param   sensor      Struct of the sensor
 * @param   counter     counter of the sensor
 */
void sensorSetCounter (sensor_t *sensor, uint16_t counter);

/**
 * @brief Sensor Get Counter: Return the value of the Counter
 * @param   sensor   Struct of the sensor
 * @return  counter of the sensor
 */
uint16_t sensorGetCounter (sensor_t *sensor);




/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/** @} doxygen end group definition */

/*==================[end of file]============================================*/
#endif /* #ifndef _SENSOR_H_ */

