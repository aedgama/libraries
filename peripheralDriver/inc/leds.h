/*************************************************************************//**
  @file     led.h
  @brief    led functions
  @author   Marcos Darino (MD)
  @version  20160530 v0.0.2 MD Name of the files.
  @version  20151109 v0.0.1 MD Initial release.
 ******************************************************************************/


/** \addtogroup DRIVER_GROUP Peripheral Drivers
 ** @{ */


/** \addtogroup LED_GROUP led
 ** @{ */



#ifndef _LED_H_
#define _LED_H_


/*==================[inclusions]=============================================*/
#include "GPIO.h"
/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif


/*==================[macros]=================================================*/

//STATES
#define     LED_ON      0  //!< Number if it is on
#define     LED_OFF     1  //!< Number if it is off
#define     UNDEFINED     3  //!< Number if it is undefined 
#

/*==================[typedef]================================================*/

typedef struct STR_led
{
   uint8_t     state;
   uint8_t     gpio;
   uint8_t     gpio_bit;
}led_t;


/*==================[external data declaration]==============================*/


/*==================[external functions declaration]=========================*/


/**
 * @brief Create the led "object" - Define the GPIO and his bit
 * @param led   Struct of the led
 * @param gpio     GPIO in the uC 
 * @param gpio_bit GPIO BIT in the uC 
 */
void ledConstructor (led_t *led, uint8_t gpio, uint8_t gpio_bit);

/**
 * @brief Init of the led, Configure the Pin function and set it as input
 * @param led   Struct of the led
 * @param port     PORT in the uC
 * @param port_pin PORT PIN in the uC
 * @param function Function of the PORT PIN
 */
void ledInit (led_t *led, uint8_t  port, uint8_t  port_pin ,uint8_t function);

/**
 * @brief Turn on the led
 * @param led   Struct of the led
  */
void led_SetON (led_t *led);

/**
 * @brief Turn of the LED
 * @param led   Struct of the led
 */
void led_SetOFF (led_t *led);

/**
 * @brief Toggle the LED
 * @param led   Struct of the led
 */
void led_SetToggle (led_t *led);




/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef _LED_H_ */





//EXAMPLE FOR THE EDU - CIAA //
//
/*
  //Led red
  ledConstructor(&ledRed,5,0);
  ledInit(&led1,2,0,FUNC0);
  //Led Green
  ledConstructor(&ledGreen,5,1);
  ledInit(&led1,2,1,FUNC0);
  //Led Blue
  ledConstructor(&ledBlue,5,2);
  ledInit(&led1,2,2,FUNC0);
  //Led 1
  ledConstructor(&led1,0,14);
  ledInit(&led1,2,10,FUNC0);
  //Led 2
  ledConstructor(&led2,1,11);
  ledInit(&led1,2,11,FUNC0);
  //Led 3
  ledConstructor(&led3,1,12);
  ledInit(&led1,2,12,FUNC0);
*/

