/*************************************************************************//**
  @file     relay.h
  @brief    relay functions
  @author   Marcos Darino (MD)
  @version  20160530 v0.0.2 MD Name of the files.
  @version  20151109 v0.0.1 MD Initial release.
 ******************************************************************************/


/** \addtogroup DRIVER_GROUP Peripheral Drivers
 ** @{ */


/** \addtogroup RELAY_GROUP relay
 ** @{ */



#ifndef _RELAY_H_
#define _RELAY_H_


/*==================[inclusions]=============================================*/
#include "GPIO.h"
/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif


/*==================[macros]=================================================*/

//STATES
#define     RELAY_ON      0  //!< Number if it is on
#define     RELAY_OFF     1  //!< Number if it is off
#define     UNDEFINED     3  //!< Number if it is undefined 
#

/*==================[typedef]================================================*/

typedef struct STR_relay
{
   uint8_t     state;
   uint8_t     gpio;
   uint8_t     gpio_bit;
} relay_t;


/*==================[external data declaration]==============================*/


/*==================[external functions declaration]=========================*/


/**
 * @brief Create the relay "object" - Define the GPIO and his bit
 * @param relay   Struct of the relay
 * @param gpio     GPIO in the uC
 * @param gpio_bit GPIO BIT in the uC
 */
void relayConstructor (relay_t *relay, uint8_t gpio, uint8_t gpio_bit);

/**
 * @brief Init of the relay, Configure the Pin function and set it as input
 * @param relay   Struct of the relay
 * @param port     PORT in the uC
 * @param port_pin PORT PIN in the uC
 * @param function Function of the PORT PIN
 */
void relayInit (relay_t *relay, uint8_t  port, uint8_t  port_pin , uint8_t function);

/**
 * @brief Turn on the relay
 * @param relay   Struct of the relay
  */
void relay_SetON (relay_t *relay);

/**
 * @brief Turn of the LED
 * @param relay   Struct of the relay
 */
void relay_SetOFF (relay_t *relay);

/**
 * @brief Toggle the LED
 * @param relay   Struct of the relay
 */
void relay_SetToggle (relay_t *relay);




/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef _RELAY_H_ */

