/*************************************************************************//**
  @file     I2C.c
  @brief    Control the I2C peripheral and channels configurations. It uses the LPCOPEN's librarys
  @author   Marcos Darino (MD)
 ******************************************************************************/


/** \addtogroup HAL_GROUP HAL
 ** @{ */


/** \addtogroup I2C_GROUP I2C
 ** @{ */



/*==================[inclusions]=============================================*/
#include "I2C.h"
/*==================[macros and definitions]=================================*/




/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/


void i2cInitModule(I2C_ID_T   id, uint32_t clockrate)
{
  Chip_I2C_Init(id);
  Chip_I2C_SetClockRate(id, clockrate);
  NVIC_EnableIRQ(I2S0_IRQn);
}


void i2cIRQ(I2C_ID_T   id)
{
  Chip_I2C_MasterStateHandler(id);
}


void i2cInit(i2c_t *i2c, I2C_ID_T id, uint8_t addr)
{
  i2c->id = id;
  i2c->addr = addr;
}


void i2cSendData(i2c_t *i2c, uint8_t *buff, uint8_t len)
{
  Chip_I2C_MasterSend(i2c->id, i2c->addr, buff, len);
}


void i2cReadData(i2c_t *i2c, uint8_t *buff, uint8_t len)
{
  Chip_I2C_MasterRead(i2c->id, i2c->addr, buff, len);
}


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
