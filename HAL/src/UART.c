/*************************************************************************//**
  @file     UART.c
  @brief    Control the UART.. It uses the LPCOPEN's librarys
  @author   Marcos Darino (MD)
 ******************************************************************************/

/** \addtogroup HAL_GROUP HAL
 ** @{ */


/** \addtogroup UART_GROUP UART
 ** @{ */


/*==================[inclusions]=============================================*/
#include "UART.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/**
 * @brief Create the UART
 */
void uartConstructor (uart_t *uart, LPC_USART_T *uartNum, RINGBUFF_T *rrb, RINGBUFF_T *trb,void *rxBuf,void *txBuf)
{
   uart->uartNum=uartNum;
   uart->rxBuf=rxBuf;
   uart->txBuf=txBuf;
   uart->rrb=rrb;
   uart->trb=trb;
}


/**
 * @brief Init the UART
 */
void uartInit (uart_t *uart, uint32_t baud, uint32_t size)
{
   Chip_UART_Init(uart->uartNum);
   Chip_UART_SetBaud(uart->uartNum, baud);
   Chip_UART_TXEnable(uart->uartNum);

   if(uart->uartNum==LPC_USART2)
   {
    //USB
   Chip_SCU_PinMux(7, 1, MD_PDN, FUNC6);              /* P7_1: UART2_TXD */
   Chip_SCU_PinMux(7, 2, MD_PLN|MD_EZI|MD_ZI, FUNC6); /* P7_2: UART2_RXD */
    //BLUETOOTH
   Chip_SCU_PinMux(1, 15, MD_PDN, FUNC1);              /* P7_1: UART2_TXD */
   Chip_SCU_PinMux(1, 16, MD_PLN|MD_EZI|MD_ZI, FUNC1); /* P7_2: UART2_RXD */

   Chip_UART_IntEnable(LPC_USART2, UART_IER_RBRINT);
   NVIC_EnableIRQ(USART2_IRQn);   
   }

   if(uart->uartNum==LPC_USART3)
   {
    //SERIAL
   Chip_SCU_PinMux(2, 3, MD_PDN, FUNC2);              /* P7_1: UART2_TXD */
   Chip_SCU_PinMux(2, 4, MD_PLN|MD_EZI|MD_ZI, FUNC2); /* P7_2: UART2_RXD */

   Chip_UART_IntEnable(LPC_USART3, UART_IER_RBRINT);
   NVIC_EnableIRQ(USART3_IRQn);   
   }

   RingBuffer_Init(uart->trb, uart->txBuf, 1, size);
   RingBuffer_Init(uart->rrb, uart->rxBuf, 1, size);   

}

/**
 * @brief Send/receive the information - handler of the uartIRQ
 */
void uartIRQ(uart_t *uart)
{
   Chip_UART_IRQRBHandler(uart->uartNum, uart->rrb, uart->trb);
}


/**
 * @brief Send data
 */
int uartSendData(uart_t *uart, void *data, uint32_t sizeData)
{
   return Chip_UART_SendRB(uart->uartNum,uart->trb, data, sizeData);
}


/**
 * @brief Receive data
 */
int uartReadData(uart_t *uart, void *data, uint32_t sizeData)
{
   return Chip_UART_ReadRB(uart->uartNum,uart->rrb, data, sizeData);
}


/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/






/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

