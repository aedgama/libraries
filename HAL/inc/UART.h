/*************************************************************************//**
  @file     UART.h
  @brief    Control the UART.. It uses the LPCOPEN's librarys
  @author   Marcos Darino (MD)

  @version  20160530 v0.0.1   MD first version
 ******************************************************************************/



#ifndef _UART_H_
#define _UART_H_

/** \addtogroup HAL_GROUP HAL
 ** @{ */


/** \addtogroup UART_GROUP UART
 ** @{ */



/*==================[inclusions]=============================================*/
#include "chip.h"

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/
typedef struct uart_STR
{
   LPC_USART_T    *uartNum;
   RINGBUFF_T     *rrb;
   RINGBUFF_T     *trb;
   void           *rxBuf;
   void           *txBuf;
}uart_t;

typedef uint8_t      uart_Buffer_t;
typedef RINGBUFF_T   uart_RingBuffer_t; 


/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/


/**
 * @brief Create the UART
 * @param uart       Struct of the uart
 * @param uartNum    UART number
 * @param rrb        RX Ring Buffer  <-
 * @param trb        TX Ring Buffer   ->
 * @param rxBuf      RX Bufer <-
 * @param txBuf      TX Bufer ->
 */
void uartConstructor (uart_t *uart, LPC_USART_T *uartNum, RINGBUFF_T *rrb, RINGBUFF_T *trb,void *rxBuf,void *txBuf);
/**
 * @brief Init the UART
 * @param uart     Struct of the uart
 * @param baud     Baud rate of the communication
 * @param size     Buffers size
 */
void uartInit (uart_t *uart, uint32_t baud, uint32_t size);


/**
 * @brief Send/receive the information - handler of the uartIRQ
 * @param uart     Struct of the uart
 */

void uartIRQ(uart_t *uart);


/**
 * @brief Send data
 * @param uart       Struct of the uart
 * @param data       Data to send
 * @param sizeData   Size of the data
 */
int uartSendData(uart_t *uart, void *data, uint32_t sizeData);

/**
 * @brief Receive data
 * @param uart       Struct of the uart
 * @param data       Data to receive
 * @param sizeData   Size of the data
 */
int uartReadData(uart_t *uart, void *data, uint32_t sizeData);




/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef _GPIO_H_ */

