/*************************************************************************//**
  @file     ADC.h
  @brief    Control the ADC peripheral and channels configurations. It uses the LPCOPEN's librarys
  @author   Pablo Llull (PL)  &  Marcos Darino (MD)
  
  @version  20160530 v0.0.3   MD Name of the files
  @version  20151126 v0.0.2   MD add new functions and comments
  @version  20151124 v0.0.1   PL first version

 ******************************************************************************/


#ifndef _ADC_H_
#define _ADC_H_


/** \addtogroup HAL_GROUP HAL
 ** @{ */


/** \addtogroup ADC_GROUP ADC
 ** @{ */



/*==================[inclusions]=============================================*/
#include "chip.h"

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/**
 * @brief Init the Analog ADC
 */
void Analog_Init(void);

/**
 * @brief Get the ADC value
 * @param  channel : ADC channel to read
 * @return The value of the ADC
 */
int16_t Analog_Read(int32_t channel);


/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef _ADC_H_ */

