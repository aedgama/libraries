/*************************************************************************//**
  @file     I2C.h
  @brief    Control the I2C ports and PIN configurations.. It uses the LPCOPEN's librarys
  @author   Marcos Darino (MD)
  
  @version  20160607 v0.0.1   MD first version
 ******************************************************************************/



#ifndef _I2C_H_
#define _I2C_H_

/** \addtogroup HAL_GROUP HAL
 ** @{ */


/** \addtogroup I2C_GROUP I2C
 ** @{ */


/*==================[inclusions]=============================================*/
#include "chip.h"

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/
typedef struct i2c_STR
{
  I2C_ID_T       id;
  uint8_t        addr;
  uint8_t        *buff;
  uint8_t        *len;
} i2c_t;
/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/


void i2cInitModule(I2C_ID_T   id, uint32_t clockrate);

void i2cIRQ(I2C_ID_T   id);

void i2cInit(i2c_t *i2c, I2C_ID_T id, uint8_t addr);

void i2cSendData(i2c_t *i2c, uint8_t *buff, uint8_t len);

void i2cReadData(i2c_t *i2c, uint8_t *buff, uint8_t len);



/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef _GPIO_H_ */

