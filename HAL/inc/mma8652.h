/*************************************************************************//**
  @file     mma8652.h
  @brief    Control the accelerometer mma8652. 
  @author   Marcos Darino (MD)
  
  @version  20160607 v0.0.1   MD first version
 ******************************************************************************/



#ifndef _MMA8652_H_
#define _MMA8652_H_

/** \addtogroup HAL_GROUP HAL
 ** @{ */


/** \addtogroup MMA8652_GROUP MMA8652
 ** @{ */


/*==================[inclusions]=============================================*/
#include "chip.h"

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/
typedef struct i2c_STR
{
  I2C_ID_T       id;
  uint8_t        addr;
  uint8_t        *buff;
  uint8_t        *len;
} i2c_t;
/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef _GPIO_H_ */

